FROM ubuntu:focal

ENV MONERO_VERSION=v0.17.1.7
ENV MONERO_SHA256=98ce0d22db0d1112114bbad4c9773d1490d30e5c643423c2e5bffc19553207f9

RUN apt-get update && apt-get install -y coreutils wget

RUN cd /tmp && wget https://downloads.getmonero.org/cli/monero-linux-x64-${MONERO_VERSION}.tar.bz2 \
	&& [ "$MONERO_SHA256  /tmp/monero-linux-x64-${MONERO_VERSION}.tar.bz2" = "$(sha256sum /tmp/monero-linux-x64-${MONERO_VERSION}.tar.bz2)" ] \
	&& tar xf monero-linux-x64-${MONERO_VERSION}.tar.bz2 \
	&& cp monero-x86_64-linux-gnu-${MONERO_VERSION}/monero* /usr/local/bin/ \
	&& useradd -ms /bin/bash monerod

USER monerod

VOLUME /home/monerod/.bitmonero

CMD /usr/local/bin/monerod --non-interactive

